<?php

/*
 * Copyright 2015 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Google\Cloud\Samples\Bookshelf;

/*
 * Adds all the controllers to $app.  Follows Silex Skeleton pattern.
 */
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Google\Cloud\Samples\Bookshelf\DataModel\DataModelInterface;

$app->get('/', function (Request $request) use ($app) {
    return $app->redirect('/books/');
});

// [START index]
$app->get('/books/', function (Request $request) use ($app) {
    //echo "Inside /books"; // FOR TESTING ONLY !!
    
    //echo "Inside /books - request: ".$request; // FOR TESTING ONLY !!    
    
    //echo "Inside /books - app: ".$app; // FOR TESTING ONLY !!
    
    /** @var DataModelInterface $model */
    $model = $app['bookshelf.model'];
    
    echo "Inside /books - model: ".$model; // FOR TESTING ONLY !!
    
    /** @var Twig_Environment $twig */
    $twig = $app['twig'];
    
    //echo "Inside /books - twig: ".$twig; // FOR TESTING ONLY !!    
    
    $token = $request->query->get('page_token');
    
    //echo "Inside /books - token: ".$token; // FOR TESTING ONLY !!      
    
    //die(); // FOR TESTING ONLY !!    
    
    $bookList = $model->listBooks($app['bookshelf.page_size'], $token);

    return $twig->render('list.html.twig', array(
        'books' => $bookList['books'],
        'next_page_token' => $bookList['cursor'],
    ));
});
// [END index]




// [START hello]
// web/index.php
//require_once __DIR__.'/../vendor/autoload.php';

//$app = new Silex\Application();

$app->get('/hello/{name}', function ($name) use ($app) {
    return 'Hello '.$app->escape($name);
});

//$app->run();
// [END hello]



// [START test_database_connection]
$app->get('/test_database_connection', function () use ($app) {
    # Fill our vars and run on cli
    # $ php -f db-connect-test.php
    $dbname = 'bookshelf';
    $dbuser = 'bookshelfuser';
    $dbpass = 'secret';
    $dbhost = '35.187.7.67';
    $link = mysqli_connect($dbhost, $dbuser, $dbpass) or die("Unable to Connect to '$dbhost'");
    mysqli_select_db($link, $dbname) or die("Could not open the db '$dbname'");
    $test_query = "SHOW TABLES FROM $dbname";
    $result = mysqli_query($link, $test_query);
    $tblCnt = 0;
    while($tbl = mysqli_fetch_array($result)) {
      $tblCnt++;
      echo $tbl[0]."<br />\n";
    }
    if (!$tblCnt) {
      echo "There are no tables<br />\n";
    } else {
      echo "There are $tblCnt tables<br />\n";
    }   
});
// [END test_database_connection]

// [START add]
$app->get('/books/add', function () use ($app) {
    /** @var Twig_Environment $twig */
    $twig = $app['twig'];

    return $twig->render('form.html.twig', array(
        'action' => 'Add',
        'book' => array(),
    ));
});

$app->post('/books/add', function (Request $request) use ($app) {
    /** @var DataModelInterface $model */
    $model = $app['bookshelf.model'];
    $book = $request->request->all();
    if (!empty($book['publishedDate'])) {
        $d = new \DateTime($book['publishedDate']);
        $book['publishedDate'] = $d->setTimezone(
            new \DateTimeZone('UTC'))->format("Y-m-d\TH:i:s\Z");
    }
    $id = $model->create($book);

    return $app->redirect("/books/$id");
});
// [END add]

// [START show]
$app->get('/books/{id}', function ($id) use ($app) {
    /** @var DataModelInterface $model */
    $model = $app['bookshelf.model'];
    $book = $model->read($id);
    if (!$book) {
        return new Response('', Response::HTTP_NOT_FOUND);
    }
    /** @var Twig_Environment $twig */
    $twig = $app['twig'];

    return $twig->render('view.html.twig', array('book' => $book));
});
// [END show]

// [START edit]
$app->get('/books/{id}/edit', function ($id) use ($app) {
    /** @var DataModelInterface $model */
    $model = $app['bookshelf.model'];
    $book = $model->read($id);
    if (!$book) {
        return new Response('', Response::HTTP_NOT_FOUND);
    }
    /** @var Twig_Environment $twig */
    $twig = $app['twig'];

    return $twig->render('form.html.twig', array(
        'action' => 'Edit',
        'book' => $book,
    ));
});

$app->post('/books/{id}/edit', function (Request $request, $id) use ($app) {
    $book = $request->request->all();
    $book['id'] = $id;
    /** @var DataModelInterface $model */
    $model = $app['bookshelf.model'];
    if (!$model->read($id)) {
        return new Response('', Response::HTTP_NOT_FOUND);
    }
    if (!empty($book['publishedDate'])) {
        $d = new \DateTime($book['publishedDate']);
        $book['publishedDate'] = $d->setTimezone(
            new \DateTimeZone('UTC'))->format("Y-m-d\TH:i:s\Z");
    }
    if ($model->update($book)) {
        return $app->redirect("/books/$id");
    }

    return new Response('Could not update book');
});
// [END edit]

// [START delete]
$app->post('/books/{id}/delete', function ($id) use ($app) {
    /** @var DataModelInterface $model */
    $model = $app['bookshelf.model'];
    $book = $model->read($id);
    if ($book) {
        $model->delete($id);

        return $app->redirect('/books/', Response::HTTP_SEE_OTHER);
    }

    return new Response('', Response::HTTP_NOT_FOUND);
});
// [END delete]
