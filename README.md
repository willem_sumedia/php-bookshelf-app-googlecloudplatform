# README #

Based on 'PHP Bookshelf App' at https://cloud.google.com/php/getting-started/tutorial-app

Reference material: https://silex.sensiolabs.org/doc/2.0/intro.html

# Bookshelf Tutorial

The Bookshelf tutorial has several parts that demonstrate how the sample app uses various Cloud Platform services.

The structured data part of the tutorial demonstrates how the sample app stores book information in a SQL or NoSQL database. The app's web page displays a form where the user can enter the title, author, description, and publication date of a book. For each book entered, the app stores this information in a database, so it can be retrieved later for viewing or editing. For this step of the tutorial, you have your choice of three databases: Cloud SQL, Cloud Datastore, or MongoDB. After you complete this step with one of the databases, you can move on to the next step.

The Cloud Storage part of the tutorial demonstrates how the sample app stores binary data in Cloud Storage. On the app's web page, the user can specify a cover image for each book. The app stores the cover images in a Cloud Storage bucket.

The authorization part of the tutorial demonstrates how the app provides a sign-in flow for the user. When a user is signed in, any books entered are associated with the individual user. Signed-in users see only their own books.

The logging part of the tutorial demonstrates how the app writes logs that become visible in the Google Cloud Platform Console. Logs of this type can provide diagnostic information during app development.

## Using Structured Data with PHP

NOTE: This repository is based on the 'Structured Data' project from the [Google Cloud Platform - Getting started with PHP](https://github.com/GoogleCloudPlatform/getting-started-php/archive/master.zip) repository.

Google Cloud SQL
Managed MySQL

- Create and configure your databases. Cloud SQL handles all other management tasks like replication and patch management.

- Access your data in Cloud SQL databases using all the tools and applications already built for MySQL.

- Use your favorite MySQL tools and apps because Cloud SQL is MySQL in the cloud.

- Move your data in and out of the cloud either by importing or exporting, or by replication with your on-premises databases.

See [Google Cloud SQL](https://cloud.google.com/sql/) for more information.

## Using Cloud SQL with PHP

See also https://cloud.google.com/php/getting-started/using-cloud-sql

Creating a Cloud SQL instance and database

### Install the SQL proxy

Download and install the Cloud SQL Proxy. The Cloud SQL Proxy is used to connect to your Cloud SQL instance when running locally.

***OS X 32-Bit***:

Download the proxy:

```javascript
curl -o cloud_sql_proxy https://dl.google.com/cloudsql/cloud_sql_proxy.darwin.386
```

Make the proxy executable:

```javascript
chmod +x cloud_sql_proxy
```

## Create a Cloud SQL instance

Create a Cloud SQL Second Generation instance.

Name the instance ***library*** or similar. It can take a few minutes for the instance to be ready. After the instance is ready, it should be visible in the instances list.

Note: Be sure to create a Second Generation instance.

If you haven't already, set the password for the MySQL root user on your Cloud SQL instance. You can do this from the command-line with the Cloud SDK (where YOUR_INSTANCE_NAME is 'library' and YOUR_INSTANCE_ROOT_PASSWORD is 'secret'):

```javascript
gcloud sql instances set-root-password [YOUR_INSTANCE_NAME] --password [YOUR_INSTANCE_ROOT_PASSWORD]
```

Now use the Cloud SDK from command-line to run the following command. Copy the connectionName value for the next step (where YOUR_INSTANCE_NAME is 'library').

```javascript
gcloud beta sql instances describe [YOUR_INSTANCE_NAME]
```

Here connectionName: verkeerslicht-165607:europe-west1:library

### Initialize your Cloud SQL instance

1. Start the Cloud SQL Proxy using the connectionName from the previous step.

[Mac OS X, type this locally from where you installed the Cloud SQL Proxy] 

```javascript
./cloud_sql_proxy -instances="[YOUR_INSTANCE_CONNECTION_NAME]"=tcp:3306
```

where [YOUR_INSTANCE_CONNECTION_NAME] = verkeerslicht-165607:europe-west1:library

This step establishes a connection from your local computer to your Cloud SQL instance for local testing purposes. Keep the Cloud SQL Proxy running the entire time you test your application locally.

NOTE: You may be prompted with the following:

```javascript
2017/04/24 17:07:59 google: could not find default credentials. See https://developers.google.com/accounts/docs/application-default-credentials for more information.
```

Then look here, https://developers.google.com/identity/protocols/application-default-credentials

In short; running the command ```gcloud auth application-default login``` in the Google Cloud web console terminal will make your identity available to the Application Default Credentials as proxy when testing application code locally. An application_default_credentials.json file will be created. Make a copy of the content of this file and create a file with the same name locally on your computer, e.g. ~/application_default_credentials.json

It will look somewhat like this:

```javascript
{
  "client_id": "664086051850-6qr4p6gpi6hn506pt8ejuq83di341hur.apps.googleusercontent.com",
  "client_secret": "d-FL95Q19q7MQmFpd7hHD0Ty",
  "refresh_token": "1/svcrmBthvp5bbWk-KsFKI-emywSqLv8LxGFMkKXYLFs",
  "type": "authorized_user"
}
```

Before logging on to the remote SQL Proxy, export the following:

```javascript
export GOOGLE_APPLICATION_CREDENTIALS=~/application_default_credentials.json
```

Now when running: ```./cloud_sql_proxy -instances="verkeerslicht-165607:europe-west1:library"=tcp:3306```

you will be prompted with:

```javascript
2017/04/24 17:32:57 Listening on 127.0.0.1:3306 for verkeerslicht-165607:europe-west1:library
2017/04/24 17:32:57 Ready for new connections
```

2. Next you will need to create a new Cloud SQL user and database. <= WE ARE HERE !!!

a. Create a [new database using the Cloud Platform Console](https://cloud.google.com/sql/docs/mysql/create-manage-databases#creating_a_database) for your Cloud SQL instance library. For example you can use the name ***bookshelf***.

b. Create a [new user using the Cloud Platform Console](https://cloud.google.com/sql/docs/mysql/create-manage-users#creating) for your Cloud SQL instance library. For example you can use the name ***bookshelfuser*** with password ***secret***.

### Configuring settings

1. Open config/settings.yml for editing.

2. Replace YOUR_PROJECT_ID with your project ID (e.g. verkeerslicht-165607). 

NOTE: You can get your ***google_client_id*** and ***google_client_secret*** from your previously saved ~/application_default_credentials.json

3. Set the value of bookshelf_backend to cloudsql.

4. Set the values of mysql_dsn, mysql_user, mysql_password, and cloudsql_connection_name to the appropriate values for your Cloud SQL instance. For example:

```javascript
mysql_dsn: "mysql:host=35.187.7.67;port=3306;dbname=bookshelf" # NOTE: the IPv4 address is taken from the SQL > library page of the Google Cloud Platform console. When running locally, use 127.0.0.1 instead.
mysql_user: bookshelfuser
mysql_password: secret
cloudsql_connection_name: "verkeerslicht-165607:europe-west1:library"
```

5. Save and close settings.yml.

### Installing dependencies

In the root of the local clone of the repository, enter this command.

```javascript
composer install
```

### Running the app on your local machine

Start a local web server:

```javascript
php -S localhost:8090 -t web
```

In your web browser, enter this address.

```javascript
http://localhost:8090
```

Now you can browse the app's web pages and add, edit, and delete books.

***NOTE***: For the application to work without error, temporarily disable the Date Published logic.

See https://blog.serverdensity.com/handling-timezone-conversion-with-php-datetime/

### Using Google Cloud Shell

Open the Google Cloud Shell on the Web console using the [>_] button.

You will be prompted as follows:

```javascript
Welcome to Cloud Shell! Type "help" to get started.
willem@verkeerslicht-165607:~$
```

where 'willem' is the user with which you are logged in, and 'verkeerslicht-165607' is the project name.

2. Clone the sample code

Use Cloud Shell to clone and navigate to the code. The application code is cloned from your project repository to the Cloud Shell.

In Cloud Shell enter:

```javascript
CLONEDTODIR=~/src/verkeerslicht-165607/php-bookshelf-app-googlecloudplatform
```

Clone a sample repository:

```javascript
git clone https://willem_sumedia@bitbucket.org/willem_sumedia/php-bookshelf-app-googlecloudplatform.git $CLONEDTODIR
```

You will be prompted with:

```javascript
Cloning into '/home/willem/src/verkeerslicht-165607/php-bookshelf-app-googlecloudplatform'...
remote: Counting objects: 53, done.
remote: Compressing objects: 100% (43/43), done.
remote: Total 53 (delta 15), reused 0 (delta 0)
Unpacking objects: 100% (53/53), done.
```

Switch to the tutorial directory:

```javascript
cd $CLONEDTODIR
```

Or, if you have cloned into the directory before, pull the sample repository instead:

```javascript
git pull
```

You will be prompted with:

```javascript
willem@verkeerslicht-165607:~/src/verkeerslicht-165607/php-bookshelf-app-googlecloudplatform$
```

### Deploying the app to the App Engine flexible environment

NOTE: Make sure you have completed the steps of the previous section; "Using Google Cloud Shell".

1. Deploy the sample app, by typing in the online Cloud Shell console :

```javascript
gcloud app deploy --project verkeerslicht-165607
```

You will be prompted with:

```javascript
You are about to deploy the following services:
 - verkeerslicht-165607/default/20170425t104700 (from [/home/willem/src/verkeerslicht-165607/php-bookshelf-app-googlecloudplatform/app.yaml])
     Deploying to URL: [https://verkeerslicht-165607.appspot.com]

Do you want to continue (Y/n)?
```

Type: Y (followed by ENTER)

You will be prompted:

```javascript
...
DONE
----------------------------------------------------------------------------------------------------------------------------------

Updating service [default]...\Stopping version [verkeerslicht-165607/default/20170424t153456].                                    
Updating service [default]...|Sent request to stop version [verkeerslicht-165607/default/20170424t153456]. This operation may take some time to complete. If you would like to verify that it succeeded, run:
  $ gcloud app versions describe -s default 20170424t153456
until it shows that the version has stopped.
Updating service [default]...done.                                                                                                
Deployed service [default] to [https://verkeerslicht-165607.appspot.com]

You can stream logs from the command line by running:
  $ gcloud app logs tail -s default

To view your application in the web browser run:
  $ gcloud app browse
```

Type:

```javascript
gcloud app browse
```
You will be prompted:

```javascript
Opening [https://verkeerslicht-165607.appspot.com] in a new tab in your default browser.
https://verkeerslicht-165607.appspot.com
```

Open a web browser and browse to https://verkeerslicht-165607.appspot.com

You should see the bookshelf app and will be able to add, edit and remove books.

NOTE: If instead you get the following message:

```Whoops, looks like something went wrong```

you most likely didn't create and complete the following file on the repository:

/config/settings.yml:

with the content as specified earlier on in this document. 

This file is 'ignored' when pushing from a clone back to the repository, to prevent overwrites. 

Simply add it and do a 'git pull' in the repository path (~/src/verkeerslicht-165607/php-bookshelf-app-googlecloudplatform) on the Google Cloud Platform inside the project on the online Cloud Shell terminal, followed by:

```javascript
gcloud app deploy --project verkeerslicht-165607
```

```javascript
gcloud app browse
```

### Understanding the code

To understand how it all works, look towards the bottom of the following page:

https://cloud.google.com/php/getting-started/using-cloud-sql

### Installing MySQL Client

See https://cloud.google.com/sql/docs/mysql/connect-admin-ip